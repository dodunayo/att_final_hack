# ATT_FINAL_HACK

Project Description:

This project takes full ML/CV models in the forms of CTNK and Darknets and with help from the Microsoft Research Team, it compress it into an ELL format that is compatible for the Raspberry Pi. 
The Project is then able to take images from the Raspberry Pi and pass it to the compressed model and receive information back from it.

How to Run:

    1) Set Up your Raspberry Pis
    
    2) Change directories into the folder that contains: "CaptureImages.py"
    
    3) Run CaptureImages.py